#!/usr/bin/ruby

# http://crysol.github.io/recipe/2011-01-29/redmine-pam-authentication-plugin
# https://bitbucket.org/DavidVilla/redmine-auth-pam

# WARNING: the user running webserver must belong to the group "shadow"

require 'rubygems'
require 'rpam'
include Rpam


class AuthSourcePam < AuthSource

  def authenticate(login, password)
    return nil if login.blank? or password.blank? or not authpam(login, password)
    return [:firstname => login]
  end

  def auth_method_name
    return "PAM"
  end
end
